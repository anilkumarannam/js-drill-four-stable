const addDefaultProps = (inputObject, defaultProps) => {
    if (inputObject && defaultProps) {
        for (let key in defaultProps) {
            if (!(key in inputObject) || !inputObject[key]) {
                inputObject[key] = defaultProps[key];
            }
        }
        return inputObject;
    }
    return {};
}

module.exports = addDefaultProps;