const getObjectKeys = object => {
    const objectKeys = [];
    if (object) {
        for (let key in object) {
            objectKeys.push(key);
        }
        return objectKeys;
    }
    return objectKeys;
}

module.exports = getObjectKeys;