const keys = require("./keys");

const mapObject = (object, callBackFunction) => {
    const mappedObject = {}
    if (object && callBackFunction) {
        const objectKeys = keys(object);
        for (let index = 0; index < objectKeys.length; index++) {
            let key = objectKeys[index];
            mappedObject[key] = callBackFunction(object[key], key);
        }
        return mappedObject;
    }
    return mappedObject;
}

module.exports = mapObject;