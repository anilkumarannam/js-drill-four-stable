const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const testObject1 = { name: undefined, age: 36, location: 'Gotham' };

const addDefaultProps = require("../defaults");

const newObject1 = addDefaultProps(testObject, { age: 23, gender: 'Male', contact: "9123456789" });
//Returns : { name: 'Bruce Wayne', age: 36, location: 'Gotham', gender: 'Male', contact : '9123456789' }
const newObject2 = addDefaultProps(undefined, { age: 23, gender: "Male" });
//Returns : {}
const newObject3 = addDefaultProps(testObject1, { name: "Abcd" });
//Returns : { name: 'Bruce Wayne', age: 36, location: 'Gotham' }
const newObject4 = addDefaultProps({}, undefined);
//Returns : {}
const newObject5 = addDefaultProps(testObject, {});
//Returns : { name: 'Bruce Wayne', age: 36, location: 'Gotham' }
const newObject6 = addDefaultProps({}, {});
//Returns : {}
const newObject7 = addDefaultProps(undefined, undefined);
//Returns : {}

console.log(newObject1);
console.log(newObject2);
console.log(newObject3);
console.log(newObject4);
console.log(newObject5);
console.log(newObject6);
console.log(newObject7);