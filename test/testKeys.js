const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const getObjectKeys = require("../keys");

console.log(getObjectKeys(testObject));
// Prints : [ 'name', 'age', 'location' ]
console.log(getObjectKeys(undefined));
// Prints : []
console.log(getObjectKeys(null));
// Prints : []