const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const getObjectValues = require("../values");

console.log(getObjectValues(testObject));
// Prints : [ 'Bruce Wayne', 36, 'Gotham' ]
console.log(getObjectValues({}));
// Prints : []
console.log(getObjectValues(undefined));
// Prints : []
console.log(getObjectValues(null));
// Prints : []