const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const mapObject = require("../mapObject");

const newObject1 = mapObject(testObject, (value, key) => value + value);
const newObject2 = mapObject(undefined, (value, key) => {value + value});
const newObject3 = mapObject(null, (value, key) => value + value);

console.log(newObject1);
// Prints : { name: 'Bruce WayneBruce Wayne', age: 72, location: 'GothamGotham' }
console.log(newObject2);
// Prints : []
console.log(newObject3);
// Prints : []