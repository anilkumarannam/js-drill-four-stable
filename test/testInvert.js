const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const invertObjectKeyValuePairs = require("../invert");

const invertedKeyValuePairs1 = invertObjectKeyValuePairs(testObject);
// Returns :  [ 'Bruce Wayne', 'name' ], [ 36, 'age' ], [ 'Gotham', 'location' ] ]
const invertedKeyValuePairs2 = invertObjectKeyValuePairs(undefined);
// Returns : []
const invertedKeyValuePairs3 = invertObjectKeyValuePairs(null);
// Returns : []

console.log(invertedKeyValuePairs1);
console.log(invertedKeyValuePairs2);
console.log(invertedKeyValuePairs3);