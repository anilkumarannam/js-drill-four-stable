const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const convertIntoKeyValuePairs = require("../pairs");

const keyValuePairs1 = convertIntoKeyValuePairs(testObject);
// Returns : [ [ 'name', 'Bruce Wayne' ], [ 'age', 36 ], [ 'location', 'Gotham' ] ]
const keyValuePairs4 = convertIntoKeyValuePairs({});
// Returns : []
const keyValuePairs2 = convertIntoKeyValuePairs(undefined);
// Returns : []
const keyValuePairs3 = convertIntoKeyValuePairs(null);
// Returns : []

console.log(keyValuePairs1);
console.log(keyValuePairs2);
console.log(keyValuePairs3);
console.log(keyValuePairs4);