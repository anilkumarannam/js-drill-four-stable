const keys = require("./keys");

const convertIntoKeyValuePairs = (object) => {
    const keyValuePairsArray = [];
    if (object) {
        const objectKeys = keys(object);
        for (let index = 0; index < objectKeys.length; index++) {
            let key = objectKeys[index];
            keyValuePairsArray.push([key, object[key]]);
        }
        return keyValuePairsArray;
    }
    return keyValuePairsArray;
}

module.exports = convertIntoKeyValuePairs;