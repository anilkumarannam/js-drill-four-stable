const getObjectValues = object => {
    const objectValues = [];
    if (object) {
        for (let key in object) {
            objectValues.push(object[key]);
        }
        return objectValues;
    }
    return objectValues;
}

module.exports = getObjectValues;