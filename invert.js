const keys = require("./keys");

const invertObjectKeyValuePairs = (object) => {
    const keyValuePairsArray = [];
    if (object) {
        const objectKeys = keys(object);
        for (let index = 0; index < objectKeys.length; index++) {
            let key = objectKeys[index];
            keyValuePairsArray.push([object[key], key]);
        }
        return keyValuePairsArray;
    }
    return keyValuePairsArray;
}

module.exports = invertObjectKeyValuePairs;